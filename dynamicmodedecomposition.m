% This code accompanies Joel Rosenfeld's YouTube Video on Dynamic Mode Decomposition at https://youtu.be/BoIQvGoCAmA
% Please cite the manuscript in the description of the video, and the YouTube channel http://www.thatmaththing.com/

% The data used in the video was the Cyllinder Flow data set by Brunton and Kutz. It can be found at http://databookuw.com/

% Load the DATA!
    % This DATA comes from the DMD book by Nathan Kutz et al, and Data
    % Driven Science and Engineering by Brunton and Kutz
    
    load('DATA/FLUIDS/CYLINDER_ALL.mat');
    
    W = UALL; % I always put things in W. Each column is a state at that particular timestep.
    
    h = 0; % DOES NOT MATTER... DISCRETE TIME...
    
    TotalSnapshots = size(W,2); % How many snapshots
    TotalKernels = size(W,2) - 1; % One less kernel than snapshots
% Define the Kernel Function
    mu = 1;
    
    Kernel = @(x,y) exp(-1/mu*norm(x-y)^2); % This is the SLOW way to do things.
    
    GramMatrix = zeros(TotalKernels);
    
    for i= 1:TotalKernels
        for j = 1:TotalKernels
            GramMatrix(i,j) = Kernel(W(:,i),W(:,j));
        end
    end
        
    InteractionMatrix = zeros(TotalKernels);
    
    for i = 1:TotalKernels
        for j = 1:TotalKernels
            InteractionMatrix(i,j) = Kernel(W(:,i),W(:,j+1));
        end
    end
    
% Eigen Decomposition

    [V,D] = eigs(pinv(GramMatrix)*InteractionMatrix,TotalKernels);
    
    % Normalize Vectors
    
    for i = 1:size(V,2)
        V(:,i) = V(:,i)/sqrt(V(:,i)'*GramMatrix*V(:,i));
    end
    
    KoopmanModes = (pinv(GramMatrix*V)*W(:,1:(end-1))')'; % Canceled a V' here. And Hitting this on W does everything at once.

% Show Koopman Modes

    figure
    
    for i=1:length(KoopmanModes(1,:))
        imshow(real(reshape(KoopmanModes(:,i),199,[])),[],'Colormap',parula,'InitialMagnification',200);
        pause(0.1);
    end
    
% Evaluate Basis at the Initial State

    InitialEvaluation = V'*GramMatrix(:,1);
    
% Reconstruction
    Reconstruction = KoopmanModes*diag(InitialEvaluation)*(diag(D).^(0:(TotalSnapshots-1)));

    
% Show Reconstruction


    figure
    
    for i=1:length(Reconstruction(1,:))
        imshow(real(reshape(Reconstruction(:,i),199,[])),[],'Colormap',parula,'InitialMagnification',200);
        pause(0.01);
    end